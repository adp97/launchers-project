%% Plots
%%  Presets
if fullScreen == 1
    % Full screen 
    f = figure();
    f.WindowState = 'maximized';
else
    % Custom size
    figure('Renderer', 'painters', 'Position', [20 20 1200 600])    
end

newcolors = [   
                0.3010 0.7450 0.9330    % Light blue
                0.4940 0.1840 0.5560    % Purple
                0       1       0       % Green
                0.9290 0.6940 0.1250    % Yellowish
                0 0.4470 0.7410         % Blue
                0.6350 0.0780 0.1840    % Red
                ];  
         
colororder(newcolors)
color=0;

out_dir = 'output/';

legend_phases =     {'Raising', 'Gravity Turn','Staging', 'Orbit Injection',...
                    'Ballistic flight', 'Orbiting phase','Kuru Launchsite'};


%% Earth 3D view
h = subplot(2,4,[1,2,5,6]);

set(gca,'visible','off') % No visible lines
set(gca,'ColorOrder',newcolors)
set(0,'DefaultLineLineWidth',2)

title('3D view from Kuru')
hold on;

plot3(state_raising(1,:),state_raising(2,:),state_raising(3,:),'LineWidth',4);
plot3(state_turn(1,:),state_turn(2,:),state_turn(3,:),'LineWidth',4);
plot3(state_staging(1,:),state_staging(2,:),state_staging(3,:),'LineWidth',4);
plot3(state_burn(1,:)*1e3,state_burn(2,:)*1e3,state_burn(3,:)*1e3,'LineWidth',4);
plot3(state_ap(1,:)*1e3,state_ap(2,:)*1e3,state_ap(3,:)*1e3,'LineWidth',4);
plot3(state_orbit(1,:)*1e3,state_orbit(2,:)*1e3,state_orbit(3,:)*1e3,'LineWidth',4);

[X, Y, Z] = sph2cart(deg2rad(Kuru.longitude),deg2rad(Kuru.latitude), 6371000);
plot3(X,Y,Z,'yp','MarkerSize',15); hold on;

ax = gca;               % get the current axis
ax.Clipping = 'off';    % turn clipping off

if strcmp(resolution,'low') == 1
    % Lowres
    earth_sphere('m')
    % view(43.4940, 15.1497) % View from Kuru
    % view(6.1915, 47.1176) % View initial phase of trajectory
    view(3.7764,14.0808) % Full Earth & Orbit View
elseif strcmp(resolution,'high') == 1
    % Highres
    planet3D('Earth Cloudy');
    view(15.1578,14.0377)
else
    error('Choose either "low" or "high" ')
end

set(gca,'CameraViewAngle', 12.5)
view(gca,36.5, 31) % Zenital view
zoom(gca,4)

lgd = legend(legend_phases)
lgd.FontSize = 14;
exportgraphics(findobj(h,'Type','Axes'), [out_dir, 'Earth_view.png'])

% Height vs time
orbit_end = find(t_orbit < 1900,1,'last');

h = subplot(2,4,3);
plot(t_raising, h_raising*1e-3); hold on
plot(t_turn,h_turn*1e-3); 
plot(t_staging,h_staging*1e-3); 
plot(t_burn,h_burn-R_earth*1e-3); 
plot(t_ap,h_ap-R_earth*1e-3); 
plot(t_orbit(1:orbit_end),h_orbit(1:orbit_end)-R_earth*1e-3); 

ylim([0 1.05*max(h_orbit(1:orbit_end)-R_earth*1e-3)])

xline(0,'k--','Lift-off           ','LineWidth',2)
xline(t_burn(end),'k--','Engines shut-off          ','LineWidth',2)
xline(t_orbit(1),'k--','2nd burn','LineWidth',2,'LabelVerticalAlignment','middle')

yline(700, 'k:', 'Desired orbit (700 km)', 'LineWidth',2,'LabelHorizontalAlignment','center')

% legend('Raising phase', 'Gravity turn')
title('Height profile')
xlabel('Time [s]'); ylabel('Height [km]'); 
legend(legend_phases(1:6),'Location','best')
exportgraphics(findobj(h,'Type','Axes'), [out_dir, 'Height_profile.png'])

% Relative Speed vs time
h = subplot(2,4,4);
plot(t_raising, v_rel_raising*1e-3); hold on
plot(t_turn,v_rel_turn*1e-3);
plot(t_staging,v_rel_staging*1e-3);
plot(t_burn,v_rel_burn);
plot(t_ap, v_rel_ap); 
plot(t_orbit(1:orbit_end), v_rel_orbit(1:orbit_end)); 

xline(0,'k--','Lift-off','LineWidth',2)
xline(t_burn(end),'k--','Engines shut-off','LineWidth',2)
xline(t_orbit(1),'k--','2nd burn','LineWidth',2,'LabelVerticalAlignment','middle')


% legend('Raising phase', 'Gravity turn')
title('Velocity profile')
xlabel('Time [s]'); ylabel('Speed [km/s]');
legend(legend_phases(1:6),'Location','best')
exportgraphics(findobj(h,'Type','Axes'), [out_dir, 'Rel_velocity.png'])


% Flight angle vs time
h = subplot(2,4,7);
plot(t_raising, fl_ang_raising); hold on
plot(t_turn, fl_ang_turn); 
plot(t_staging, fl_ang_staging); 
plot(t_burn, fl_ang_burn); 
plot(t_ap, fl_ang_ap); 
plot(t_orbit(1:orbit_end), fl_ang_orbit(1:orbit_end)); 
ylim([0 99]);
xlim([0 t_orbit(orbit_end)])

% legend('Raising phase', 'Gravity turn')
title('Flight angle profile');
xlabel('Time [s]'); ylabel('Flight angle [deg]'); 
legend(legend_phases(1:6),'Location','best')
exportgraphics(findobj(h,'Type','Axes'), [out_dir, 'Flight_angle.png'])

% Density vs time
h = subplot(2,4,8);
plot(t_raising, dyn_press_raising); hold on
plot(t_turn, dyn_press_turn); 
yline(4.5e4,'r--','Label','45 KPa')

title('Dinamic pressure profile')
%legend('Raising phase', 'Gravity turn', 'Critical pressure')
xlabel('Time [s]'); ylabel('Dynamic Pressure [Pa]'); 
ylim([min(dyn_press_raising) 1.05*4.5e4])
legend(legend_phases(1:2),'Location','best')
exportgraphics(findobj(h,'Type','Axes'), [out_dir, 'Dinamic_pressure.png'])



% Custom legend
% fig = gcf;
% % fig.Position(3) = fig.Position(3)-250;
% % add legend
% Lgnd = legend('show');
% Lgnd.Position(1) = 0.3;
% Lgnd.Position(2) = 0.125;

saveas(gca,'output/General view.png')
%% Mass profile
figure()
t_all = [t_raising , t_turn, t_staging , t_burn, t_ap , t_orbit(1:orbit_end)];
Mass.all = [ Mass.raising, Mass.turn, Mass.staging, Mass.burn, Mass.ap, Mass.orbit(1:orbit_end)];

plot(t_all, Mass.all)
yline(Mass.all(end),'r-. ', ['Final mass: ', num2str(Mass.all(end),'%4.0f'), ' kg'])
yline(Mass.raising(1),'r-.', ['Initial mass: ', num2str(Mass.raising(1),'%4.0f'), ' kg'])
xlabel('Time [s]'); ylabel('Mass [kg]')
title('Launcher mass profile')
saveas(gca,'output/Mass_profile.png')

%% DV losses
Dv_thrust = isp(1)*g0*log(Mass.raising(1)/Mass.turn(end)) + ...
    isp(2)*g0*log(Mass.burn(1)/Mass.orbit(end));

Dv_drag = 0;
Dv_gravity = 0;
Dv_press_loss = 0;
for i = 2:length(t_raising)
    [~,p,~,a] = expEarthAtm(h_raising(i),0);
    Dv_drag = Dv_drag + dyn_press_raising(i)*Launcher.cd(v_rel_raising(i)/a)*...
        Launcher.S_aero/Mass.raising(i)*(t_raising(i) - t_raising(i-1));
    Dv_gravity = Dv_gravity + mu/(r_earth + ...
        h_raising(i)*1e-3)^2*1e3 * sind(fl_ang_raising(i))*(t_raising(i) - t_raising(i-1));
%     Dv_gravity = Dv_gravity + g0* sind(fl_ang_raising(i))*(t_raising(i) - t_raising(i-1));
    Dv_press_loss = Dv_press_loss + ...
        (p - 45000)*Launcher.S_nozzle/Mass.raising(i)*(t_raising(i) - t_raising(i-1));
end
for i = 2:length(t_turn)
    [~,~,~,a] = expEarthAtm(h_turn(i),0);
    Dv_drag = Dv_drag + dyn_press_turn(i)*Launcher.cd(v_rel_turn(i)/a)*...
        Launcher.S_aero/Mass.turn(i)*(t_turn(i) - t_turn(i-1));
    Dv_gravity = Dv_gravity + mu/(r_earth + ...
        h_turn(i)*1e-3)^2*1e3 * sind(fl_ang_turn(i))*(t_turn(i) - t_turn(i-1));
%     Dv_gravity = Dv_gravity + g0 * sind(fl_ang_turn(i))*(t_turn(i) - t_turn(i-1));
    Dv_press_loss = Dv_press_loss + ...
        (p - 45000)*Launcher.S_nozzle/Mass.turn(i)*(t_turn(i) - t_turn(i-1));
end
for i = 2:length(t_burn)
    Dv_gravity = Dv_gravity + mu/(r_earth + ...
        h_burn(i))^2*1e3 * sind(fl_ang_burn(i))*(t_burn(i) - t_burn(i-1));
end

% for i = 2:length(t_staging)
%     Dv_gravity = Dv_gravity + mu/(r_earth + ...
%         h_staging(i))^2*1e3 * sind(fl_ang_staging(i))*(t_staging(i) - t_staging(i-1));
% end
% for i = 2:length(t_ap)
%     Dv_gravity = Dv_gravity + mu/(r_earth + ...
%         h_ap(i))^2*1e3 * sind(fl_ang_ap(i))*(t_ap(i) - t_ap(i-1));
% end

Dv_tot_loss = Dv_drag + Dv_press_loss + Dv_gravity;
Dv_used = Dv_thrust - Dv_tot_loss;

fprintf('Thrust (kN):\n');
fprintf('First stage: %3.1f\n',m_f(1)*9.81*7*1e-3);
fprintf('Second stage: %3.1f\n',m_f(2)*9.81*5*1e-3);
fprintf('\n');

fprintf('Dv thrust %4.1f m/s\n', Dv_thrust);
fprintf('Dv losses (m/s):\n');
fprintf('Drag: %3.1f\n',Dv_drag);
fprintf('Gravity: %3.1f\n',Dv_gravity);
fprintf('Pressure: %3.1f\n',Dv_press_loss);



%% Individual figures
% Launchers profile
t_all = [t_raising , t_turn, t_staging , t_burn, t_ap , t_orbit(1:orbit_end)];

h_all = [h_raising'*1e-3 , h_turn'*1e-3 , h_staging'*1e-3, (h_burn-R_earth*1e-3)', ...
    (h_ap-R_earth*1e-3)' , (h_orbit(1:orbit_end)-R_earth*1e-3)'];

v_rel_all = [v_rel_raising'*1e-3 , v_rel_turn'*1e-3, v_rel_staging'*1e-3 , v_rel_burn',...
    v_rel_ap' , v_rel_orbit(1:orbit_end)'];

figure()
colormap(jet)
scatter(t_all,h_all,[],v_rel_all,'fill')
colorbar

ylim([0 1.05*max(h_orbit(1:orbit_end)-R_earth*1e-3)])

xline(0,'k--','Lift-off','LineWidth',2)
xline(t_burn(end),'k--','Engines shut-off','LineWidth',2)
xline(t_orbit(1),'k--','2nd burn','LineWidth',2,'LabelVerticalAlignment','middle')

title('Launcher profiles')
xlabel('Time [s]'); ylabel('Height [km]'); 
ylabel(colorbar,'Velocity [km/s]')
saveas(gca,'output/Launcher profiles.png')

%% 3D views
figure()
% resolution = 'high';

set(gca,'visible','off') % No visible lines
set(gca,'ColorOrder',newcolors)

title('3D view from Kuru')
hold on;

plot3(state_raising(1,:),state_raising(2,:),state_raising(3,:),'LineWidth',3);
plot3(state_turn(1,:),state_turn(2,:),state_turn(3,:),'LineWidth',3);
plot3(state_staging(1,:),state_staging(2,:),state_staging(3,:),'LineWidth',3);
plot3(state_burn(1,:)*1e3,state_burn(2,:)*1e3,state_burn(3,:)*1e3,'LineWidth',3);
plot3(state_ap(1,:)*1e3,state_ap(2,:)*1e3,state_ap(3,:)*1e3,'LineWidth',3);
plot3(state_orbit(1,:)*1e3,state_orbit(2,:)*1e3,state_orbit(3,:)*1e3,'LineWidth',3);

[X, Y, Z] = sph2cart(deg2rad(Kuru.longitude),deg2rad(Kuru.latitude), 6371000);
plot3(X,Y,Z,'yp','MarkerSize',15); hold on;

if strcmp(resolution,'low') == 1
    % Lowres
    earth_sphere('m')
    % view(43.4940, 15.1497) % View from Kuru
    % view(6.1915, 47.1176) % View initial phase of trajectory
    % view(3.7764,14.0808) % Full Earth & Orbit View
    % view(0.7180, 55.2371) % Zoom in initial phase
    view(-50, 90) % Zenital view
elseif strcmp(resolution,'high') == 1
    % Highres
    planet3D('Earth Cloudy');
%     view(15.1578,14.0377)
else
    error('Choose either "low" or "high" ')
end

ax = gca;               % get the current axis
ax.Clipping = 'off';    % turn clipping off

view(-50, 90) % Zenital view
zoom(2)

legend(legend_phases)
saveas(gca,'output/Zenital_View.png')

view(23, 9) % Side view
saveas(gca,'output/Zenital_View_2.png')

%% Mach profile
% mach = linspace(0,7);
% figure()
% plot(Launcher.Mach, Launcher.CD, 'r*','MarkerSize',3); hold on
% plot(mach, Launcher.cd(mach))
% xlabel('Mach'); ylabel('c_D')
% title('c_D profile')
% saveas(gca,'output/Mach_profile.png')





























%% Animation
switch animation
    case 1
        color=0;
        % Concatenate all state vectors in same units
        state_all = [state_raising*1e-3, state_turn*1e-3, state_staging*1e-3, state_burn(1:6,:),...
            state_ap(1:6,:), state_orbit(1:6,:)]*1e3;
        
        h_all = (vecnorm(state_all(1:3,:)) - R_earth)*1e-3;
        
        v_rel_all = [v_rel_raising'*1e-3, v_rel_turn'*1e-3, v_rel_staging'*1e-3, v_rel_burn', v_rel_ap', v_rel_orbit'];
        
        t_all = [t_raising, t_turn, t_staging, t_burn, t_ap, t_orbit];
        
        [t_all,index] = unique(t_all);
        
        fl_ang_all = [fl_ang_raising',fl_ang_turn(2:end)', fl_ang_staging(2:end)', fl_ang_burn(2:end)',fl_ang_ap(2:end)', fl_ang_orbit(2:end)'];
        
        dyn_press_all = [dyn_press_raising', dyn_press_turn(2:end)'];
        
        n_frames = 100;
        
        t_raising_video = linspace(0,t_raising(end),n_frames);
        t_turn_video = linspace(t_raising(end),t_turn(end),n_frames);
        t_staging_video = linspace(t_turn(end),t_staging(end),n_frames/10);
        t_burn_video = linspace(t_staging(end),t_burn(end),n_frames);
        t_ap_video = linspace(t_burn(end),t_ap(end),n_frames);
        t_orbit_video = linspace(t_ap(end),t_orbit(end),n_frames);
        
        t_video = [t_raising_video, t_turn_video, t_staging_video,...
            t_burn_video, t_ap_video, t_orbit_video];
        
        Flag.raising = length(t_raising_video);
        Flag.turn = Flag.raising + length(t_turn_video);
        Flag.staging = Flag.turn + length(t_staging_video);
        Flag.burn = Flag.staging + length(t_burn_video);
        Flag.ap = Flag.burn + length(t_ap_video);
        Flag.orbit = Flag.ap + length(t_orbit_video);
        Flag.end = Flag.ap + orbit_end;
        
        % Main view
        if fullScreen == 1
            % Full screen
            f = figure();
            f.WindowState = 'maximized';
        else
            % Custom size
            figure('Renderer', 'painters', 'Position', [20 20 1200 600])
        end
        % resolution = 'low';
        set(0,'DefaultLineLineWidth',2)
        
        % 1. Earth
        subplot(2,4,[1,2,5,6])
        title('Animation of the orbit')
        
        anim_general = gca;               % get the current axis
        set(anim_general,'visible','off') % Axis lines & background off
        % set(anim_general, 'XLim',[-15000 15000],'YLim',[-15000 15000],'ZLim',[-15000 15000]) % Limits to the view
        
        % Clipping on
%         axis(anim_general,1.0e+06*[3.4537    4.9761   -5.2536   -3.7313    0.1004    1.6228])
%         view( -139.8838, 90)
        
        
        if strcmp(resolution,'low') == 1
            % Lowres
            earth_sphere('m')
        elseif strcmp(resolution,'high') == 1
            % Highres
            planet3D('Earth Cloudy');
        else
            error('Choose either "low" or "high" ')
        end
        
        anim_rocket_line = animatedline(anim_general,'Color',newcolors(1,:),'LineWidth',2);
        anim_rocket_dot = animatedline(anim_general,'MaximumNumPoints',1,...
            'Color',newcolors(1,:),'Marker','.','MarkerSize',15);
        
        anim_general.Clipping = 'off';    % turn clipping off
        
        % view(23, 9) % Side view
        % view(36.5, 31) % Side view
        view(40, 90) % Side view
        hold on;
        
        [X, Y, Z] = sph2cart(deg2rad(Kuru.longitude),deg2rad(Kuru.latitude), 6371000);
        plot3(X,Y,Z,'yp','MarkerSize',15); hold on;
        
        % 2. Height animation
        subplot(2,4,3)
        title('Height profile')
        xlabel('Time [s]'); ylabel('Height [km]');
        % ylim([0 1.05*max(h_all)])
        
        anim_height = gca;               % get the current axis
        anim_height_line= animatedline(anim_height,'LineWidth',2,'Color',newcolors(1,:));
        
        
        % 3. Velocity animation
        subplot(2,4,4)
        title('Velocity profile')
        xlabel('Time [s]'); ylabel('Velocity [km/s]');
        
        anim_vel = gca;               % get the current axis
        anim_vel_line= animatedline(anim_vel,'LineWidth',2,'Color',newcolors(1,:));
        
        % 4. Flight Angle
        subplot(2,4,7)
        title('Flight angle profile')
        xlabel('Time [s]'); ylabel('Flight angle [deg]');
        
        anim_fangle = gca;               % get the current axis
        anim_fangle_line= animatedline(anim_fangle,'LineWidth',2,'Color',newcolors(1,:));
        
        % 5. Density
        subplot(2,4,8)
        title('Dinamic pressure profile')
        xlabel('Time [s]'); ylabel('Dynamic Pressure [Pa]');
        
        anim_density = gca;               % get the current axis
        anim_density_line= animatedline(anim_density,'LineWidth',2,'Color',newcolors(1,:));
        
        ylim([min(dyn_press_raising) 1.05*4.5e4])
        
        yline(4.5e4,'r--','Label','45 KPa')
        
        
        % Animation plots
        gif_name = [out_dir , 'animation/Animation.gif'];
        
        % Legend
        axes('pos',[.1 .15 0.7 .1]);
        imshow('output/legend.png','InitialMagnification',50)
        
        state_video = [interp1(t_all,state_all(1,index),t_video); ...
            interp1(t_all,state_all(2,index),t_video); ...
            interp1(t_all,state_all(3,index),t_video); ...
            interp1(t_all,state_all(4,index),t_video); ...
            interp1(t_all,state_all(5,index),t_video); ...
            interp1(t_all,state_all(6,index),t_video)];
        
        h_video = interp1(t_all,h_all(index),t_video);
        v_rel_video = interp1(t_all,v_rel_all(index),t_video);
        
        fl_ang_video = interp1(t_all,fl_ang_all,t_video);
        dyn_press_video = interp1(t_all(1:length(dyn_press_all)),dyn_press_all,t_video);
        
        Flag.end = find(t_video < 1200,1,'last');
        
        % Orbit animation
        for k = 1:length(t_video)
            
            % Lines of the orbit
            addpoints(anim_rocket_line,state_video(1,k),state_video(2,k),state_video(3,k));
            % Point of the rocket
            addpoints(anim_rocket_dot,state_video(1,k),state_video(2,k),state_video(3,k));
            
            if k<= Flag.end
                % Height profile
                addpoints(anim_height_line,t_video(k),h_video(k));
                % Velocity profile
                addpoints(anim_vel_line,t_video(k), v_rel_video(k));
                % Flight angle profile
                addpoints(anim_fangle_line,t_video(k), fl_ang_video(k));
            else
            end
            
            if k<= Flag.turn
                % Density profile
                addpoints(anim_density_line,t_video(k), dyn_press_video(k));
            else
            end
            
            drawnow % limitrate
            
            % Animation changes for every phase
            if k==1
                disp('Lift-off!')
                color = color + 1;
                
                % 3D view
                anim_general.Clipping = 'off';    % turn clipping off    
                set(anim_general,'CameraViewAngle', 12.5)
                axis(anim_general, 1.0e+06 * [3.2841    6.4435   -6.6680   -3.4808   -1.6691    1.5184])
                view(anim_general, 9.3117,42.6521)
                zoom(anim_general, 0.5)
                
                % Animation colors
                anim_rocket_line = animatedline(anim_general,'Color',newcolors(color,:),'LineWidth',2);
                anim_rocket_dot = animatedline(anim_general,'MaximumNumPoints',1,'Color',newcolors(color,:),'Marker','.','MarkerSize',15);
                
                anim_height_line = animatedline(anim_height,'Color',newcolors(color,:),'LineWidth',2);
                anim_vel_line = animatedline(anim_vel,'Color',newcolors(color,:),'LineWidth',2);
                
                anim_fangle_line = animatedline(anim_fangle,'Color',newcolors(color,:),'LineWidth',2);
                anim_density_line = animatedline(anim_density,'Color',newcolors(color,:),'LineWidth',2);
                
                % X-lines
                xline(anim_height,0,'k--','Lift-off           ','LineWidth',2)
                xline(anim_vel,0,'k--','Lift-off           ','LineWidth',2)
                
                zoom(anim_general,2)
                
            elseif k==Flag.raising
                disp('Raising phase...')
                color = color + 1;
                
                % Animation colors
                anim_rocket_line = animatedline(anim_general,'Color',newcolors(color,:),'LineWidth',2);
                anim_rocket_dot = animatedline(anim_general,'MaximumNumPoints',1,'Color',newcolors(color,:),'Marker','.','MarkerSize',15);
                
                anim_height_line = animatedline(anim_height,'Color',newcolors(color,:),'LineWidth',2);
                anim_vel_line = animatedline(anim_vel,'Color',newcolors(color,:),'LineWidth',2);
                
                anim_fangle_line = animatedline(anim_fangle,'Color',newcolors(color,:),'LineWidth',2);
                anim_density_line = animatedline(anim_density,'Color',newcolors(color,:),'LineWidth',2);
                
            elseif k==Flag.staging
                disp('Raising phase...')
                color = color + 1;
                
                % Animation colors
                anim_rocket_line = animatedline(anim_general,'Color',newcolors(color,:),'LineWidth',2);
                anim_rocket_dot = animatedline(anim_general,'MaximumNumPoints',1,'Color',newcolors(color,:),'Marker','.','MarkerSize',15);
                
                anim_height_line = animatedline(anim_height,'Color',newcolors(color,:),'LineWidth',2);
                anim_vel_line = animatedline(anim_vel,'Color',newcolors(color,:),'LineWidth',2);
                
                anim_fangle_line = animatedline(anim_fangle,'Color',newcolors(color,:),'LineWidth',2);
                anim_density_line = animatedline(anim_density,'Color',newcolors(color,:),'LineWidth',2);
                
                
            elseif k==Flag.turn
                disp('Gravity turn started...')
                color = color + 1;                
                
                % Animation colors
                anim_rocket_line = animatedline(anim_general,'Color',newcolors(color,:),'LineWidth',2);
                anim_rocket_dot = animatedline(anim_general,'MaximumNumPoints',1,'Color',newcolors(color,:),'Marker','.','MarkerSize',15);
                
                anim_height_line = animatedline(anim_height,'Color',newcolors(color,:),'LineWidth',2);
                anim_vel_line = animatedline(anim_vel,'Color',newcolors(color,:),'LineWidth',2);
                
                anim_fangle_line = animatedline(anim_fangle,'Color',newcolors(color,:),'LineWidth',2);
                anim_density_line = animatedline(anim_density,'Color',newcolors(color,:),'LineWidth',2);
                               
                
            elseif k== Flag.burn
                disp('Orbit injection started...')
                color = color + 1;
                
%                 view(anim_general,36.5, 31)
                anim_general.Clipping = 'off';    % turn clipping off
                axis(anim_general,1.0e+06 *[-8   8   -8   8   -8   8])
                set(anim_general,'CameraViewAngle', 12.5)
                zoom(anim_general,2)
                
                % Animation colors
                anim_rocket_line = animatedline(anim_general,'Color',newcolors(color,:),'LineWidth',2);
                anim_rocket_dot = animatedline(anim_general,'MaximumNumPoints',1,'Color',newcolors(color,:),'Marker','.','MarkerSize',15);
                
                anim_height_line = animatedline(anim_height,'Color',newcolors(color,:),'LineWidth',2);
                anim_vel_line = animatedline(anim_vel,'Color',newcolors(color,:),'LineWidth',2);
                
                anim_fangle_line = animatedline(anim_fangle,'Color',newcolors(color,:),'LineWidth',2);
                anim_density_line = animatedline(anim_density,'Color',newcolors(color,:),'LineWidth',2);
                
%                 view(anim_general,36.5, 31) % Side view
                
                % X-lines
                xline(anim_height,t_burn(end),'k--','Engines shut-off          ','LineWidth',2)
                xline(anim_vel,t_burn(end),'k--','Engines shut-off          ','LineWidth',2)
                
                
            elseif k== Flag.ap
                disp('Apoapsis reached, now orbiting...')
                color = color + 1;
                
                % Animation colors
                anim_rocket_line = animatedline(anim_general,'Color',newcolors(color,:),'LineWidth',2);
                anim_rocket_dot = animatedline(anim_general,'MaximumNumPoints',1,'Color',newcolors(color,:),'Marker','.','MarkerSize',15);
                
                anim_height_line = animatedline(anim_height,'Color',newcolors(color,:),'LineWidth',2);
                anim_vel_line = animatedline(anim_vel,'Color',newcolors(color,:),'LineWidth',2);
                
                anim_fangle_line = animatedline(anim_fangle,'Color',newcolors(color,:),'LineWidth',2);
                anim_density_line = animatedline(anim_density,'Color',newcolors(color,:),'LineWidth',2);
                
                % X-lines
                xline(anim_height,t_orbit(1),'k--','2nd burn','LineWidth',2,'LabelVerticalAlignment','middle')
                xline(anim_vel,t_orbit(1),'k--','2nd burn','LineWidth',2,'LabelVerticalAlignment','middle')
                
            elseif k== Flag.orbit
                disp('First period completed!')
                
            end
            
            if k> Flag.ap
                degRate = 360/(Flag.orbit - Flag.ap);
                [az el] = view();
                
                az = az + degRate;
                view(anim_general,az, el )
            elseif (k > Flag.turn) && (k < Flag.burn)
%                 zoom reset
%                 zoom_rate = linspace(0.5, 2 , abs(Flag.turn - Flag.burn));
%                 zoom(anim_general, zoom_rate(k - Flag.turn + 1))
                  zoom(anim_general, 0.9925)
                
            else
            end
            
            % Animation generator (.png)
%             isaninteger = @(x)isfinite(x) & x==floor(x);
%             
%                 if isaninteger(k/5 )== 1
%                     name = append('output/animation/Animation_Earth_',num2str(k),'.png');
%                     saveas(gcf,name)
%                 else
%                 end
%             
            % GIF Generator
%             frame = getframe(gcf);
%             im = frame2im(frame);
%             [imind,cm] = rgb2ind(im,256);
%             isaninteger = @(x)isfinite(x) & x==floor(x);
%             if k == 1
%                 imwrite(imind,cm,gif_name,'gif', 'Loopcount',inf);
%             elseif isaninteger(k/10 )== 1
%                 imwrite(imind,cm,gif_name,'gif','WriteMode','append');
%             else
%             end
            % Speed up the gif using: https://ezgif.com/speed    at x1500
        end
        
    otherwise
        disp('No animation :(')
        
end








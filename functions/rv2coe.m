function [A,E,Omega,i,omega,theta] = rv2coe(r,v,mu)

% Given position (r) and velocity (v) vectors in the ECI and ECI basis,
% this function returns the Euler clasical parameters

% ECI unitary vectors:
i0 = [1,0,0];
j0 = [0,1,0];
k0 = [0,0,1];

% Momentum
h = cross(r,v);

% Scalar values

R = norm(r);
V = norm(v);
H = norm(h);

% Unitary vectors in orbital plane:

ur = r/R;   % Radial
ut = v/V;   % Tangential
un = h/H;   % Normal


% Semi-mayor axis
A = 1/(2/R - V^2/mu);

% Semilatus rectum
P = H^2/mu;

% Eccentricity
e = cross(v,h)/mu - ur;
E = norm(e);

up = e/E;   % Perigee

% Radial and azimuthal components of velocity:

vr  = dot(v,ur)*ur; % Ratial
vth = v - vr;       % Azimuthal

Vr  = norm(vr);
Vth = norm(vth);

% True anomaly

sth = Vr*H/mu/E;
cth = (Vth*H/mu-1)/E;
theta = getAngle(sth,cth);

% Intermediate bases:

%S1
k1 = k0;
i1 = cross(k1,un)/norm(cross(k1,un));
j1 = cross(k1,i1);

%S2
i2 = i1;
k2 = un;
j2 = cross(k2,i2);

%S3
k3 = k2;
i3 = up;
j3 = cross(k3,i3);

% RAAN, Omega
cOmeg = dot(j0,j1);
sOmeg = -dot(i0,j1);
Omega = getAngle(sOmeg,cOmeg);

% Inclination (Only goes from 0 to pi)

ci = dot(j1,j2);
i = acos(ci);

% Argument of the periapsis, omega

comeg = dot(i2,i3);
someg = dot(j2,i3);
omega = getAngle(someg,comeg);

end

function alpha = getAngle(salpha,calpha)

% For a given sine and cosine, this function returns the angle at the right
% cuadrant

% acos function returns values from 0 to pi

if salpha >= 0 
    % First and second cuadrant
    alpha = acos(calpha);
    
else
    % Second and third cuadrant
    alpha = 2*pi - acos(calpha);
    
end

end
    
function [r,v] = coe2rv(a,e,Omega,i,omega,theta,mu)

% Given the Euler classical elements, this function returns the position
% and velocity vectors in the ECI basis

% Rotation matrices from S3 to S0;

S3_S2 = [cos(omega+theta), -sin(omega+theta), 0;... % From S3 to S2
         sin(omega+theta),  cos(omega+theta), 0;...
         0         ,  0         , 1];
     
S2_S1 = [1 , 0     , 0;...              % From S2 to S1
         0 ,  cos(i), -sin(i);
         0 ,  sin(i),  cos(i)];
     
S1_S0 = [cos(Omega), -sin(Omega), 0;... % From S1 to S0
         sin(Omega),  cos(Omega), 0;...
         0         ,  0         , 1];
     
S3_S0 = S1_S0*S2_S1*S3_S2;

% Computing position and velocity magnitudes

P     = a*(1-e^2);             % Semilatus rectum
H     = sqrt(mu*P);            % Angular momentum
R     = P/(1+e*cos(theta));    % Radius
Vr    = mu/H*e*sin(theta);     % Radial velocity 
Vthet = mu/H*(1+e*cos(theta)); % Tangential velocity


% Position vector
ur = [1;0;0];
ut = [0;1;0];
r = R*S3_S0*ur;

% Velocity vector
vr = Vr*S3_S0*ur;
vt = Vthet*S3_S0*ut;
v  = vr + vt;

end
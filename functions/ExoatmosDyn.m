function state_der = ExoatmosDyn(t,state, mu, m0, mdot, T)
% Function to compute the derivatives of the state and costate vectors
% state = [r;v;pr;pv] where
% r: position vector [km]
% v: velocity vector [km/s]
% pr: costate position
% pv: costate velocity

%% Parameters introduced:
% mu: Gravitational parameter Earth [km3/s2]
% m0: initial mass Launcher [kg]
% mdot: massflow Launcher [kg/s]
% T: thrust Launcher [kN]

state_der = zeros(12,1);
r = state(1:3);
v = state(4:6);
pr = state(7:9);
pv = state(10:12);

% State Dynamic equations
m = m0 - mdot*t; % Current mass Launcher [kg]
a = -mu/norm(r)^3*r + T/m*pv/norm(pv); % Acceleration [km/s]

state_der(1:3) = v;
state_der(4:6) = a;

% Costate Dynamic equations
pr_dot = mu/norm(r)^3 * (3*dot(r,pv)*r/norm(r)^2 - pv);

state_der(7:9) = pr_dot;
state_der(10:12) = -pr;

end
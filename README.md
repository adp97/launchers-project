# Launchers project
This repository stores the code required for the assigment of the subject Launchers & Reentry Vehicles from Master in Space Engineering at UC3M. 

![Screenshot](output/thumbnail.gif)

## Authors and acknowledgment
* **Angel del Pino Jimenez** 
* **David Alejandro Rubio Woodruff** 
* **Raúl Sánchez Moreno** 


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details




function state_der = Dynamics(t, state, Launcher, omega, mu, R_earth, ... 
    flag_flight)

% Dynamics function for the launcher
% t: time [s]
% state = [r_in;v_in] [m;m/s] Position and velocity in ECI
% Launcher: Struct with launcher characteristics
% omega: rotation of the earth in ECI [rad/s]
% flag_flight: integer indicating raising (1) or gravity turn (2)
% dens: density as a function of altitude
% temp: temperature as a function of altitude
% R_gas, heat_rat: Constants of air

state_der = zeros(6,1);

% Getting the state vector
r_in = state(1:3);
v_in = state(4:6);

%% Position derivative
state_der(1:3) = v_in; % r_dot = v
m = Launcher.init_mass - Launcher.mass_flow*t;
%% Velocity derivative
v_rel = v_in - cross(omega,r_in);

% Gravity
G = -mu*m/norm(r_in)^3 * r_in;

% Drag
h = norm(r_in) - R_earth; % Height [m]
[dens,press,~,a] = expEarthAtm(h,0);

mach = norm(v_rel)/a;
cd = Launcher.cd(mach);
D = -0.5*dens*Launcher.S_aero*cd*norm(v_rel)*v_rel;

% Thrust
T_mag = Launcher.T + (Launcher.exit_press - press)*Launcher.S_nozzle;

switch flag_flight
    case 1
        % Raising, thrust in radial direction
        T = T_mag*r_in/norm(r_in); 
    case 2
        % Gravity turn, thrust in the relative velocity direction
        T = T_mag*v_rel/norm(v_rel);
end

% Mass and acceleration


state_der(4:6) = (D+T+G)/m;

end
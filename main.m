%% Main file for Lauchers project
%
% Universidad Carlos III de Madrid
% Master in Space Engineering
% Launchers & Reentry Vehicles
%
% Authors:  Angel del Pino Jiménez          (100439358) 
%           David Alejando Rubio Woodruff   (100363403)
%           Raúl Manuel Sánchez Moreno      (100363363)
%
% Date: October 2021
%

%% Presets
clear; clc; close all
folder='functions';
addpath(genpath(folder))

%% Staging
Staging_Practice
%% Endoatmospheric flight
Endoatmospheric_Practice
%% Exoatmospheric and ballistic flight
Exoatmospheric_Practice
%% 3D views
fullScreen = 1; % Boolean: 1 fullscreen, 0 custom size
animation = 1;  % Boolean: 1 show animation, 0 no animation
resolution = 'low'; % Resolution: "high" or "low" (low is better)

Views
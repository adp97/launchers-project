%% Practice 3: optimization of the trajectory in the exoatmospheric flight
%
% Universidad Carlos III de Madrid
% Master in Space Engineering
% Launchers & Reentry
%
% Authors:  Angel del Pino Jiménez          (100439358) 
%           David Alejando Rubio Woodruff   (100363403)
%           Raúl Manuel Sánchez Moreno      (100363363)
%
% Date: October 2021
%

%% Staging
Launcher.init_mass = Mass.turn(end);
Launcher.T = 0;
Launcher.exit_press = 0;
Launcher.mass_flow = 0;

staging_time = 5;

options = odeset('RelTol', 1e-8, 'AbsTol', 1e-12);
[t_staging,state_staging] = ode45(@(t,state) EndoatmosDyn(t, state, Launcher, ...
    omega, mu, R_earth,  flag_flight), [t_turn(end),t_turn(end)+staging_time], ...
    init_state, options ) ;

t_staging = t_staging';
state_staging = state_staging';

Mass.staging = Mass.turn(end)*ones(1,length(t_staging));

%% Second burn
% Parameters
g_max = 5*g0;

mu = mu*1e-9; % Gravitational parameter Earth [km3/s2]

Launcher.T  = m_f(2)*g_max*1e-3; % Launcher's thrust [kN]
Launcher.m0 = m_0(2); % Launcher's mass [kg]
Launcher.mdot = Launcher.T*1e3/isp(2)/g0; % Launcher's massflow [kg/s]
Launcher.isp = isp(2);

tb_max = (m_0(2) - m_f(2))/Launcher.mdot*1e-2;

% Initial conditions of the launcher come from dynamics [km] and [km/s].
init_state = state_staging(:,end);

r0 = init_state(1:3)*1e-3;
v0 = init_state(4:6)*1e-3; 

g = @(r) -mu/norm(r)^3*r; % Gravity law
a0 = g(r0) - Launcher.T/Launcher.m0*v0/norm(v0); % Initial acceleration [km/s2]

% Initial guess of the costate
pv0 = v0/norm(v0); % Costate velocity
pr0 = a0/norm(v0) - v0*norm(a0)/norm(v0)^2; % Costate position
tb0 = 1; % Burning time [Non-dim]

% Bounds
up_bound = [tb_max; 10;10;10;10;10;10];
lw_bound = [0; -10;-10;-10;-10;-10;-10];

% Optimization
optVars0 = [tb0;pr0;pv0];
options = optimset('TolFun', 1e-3);

optVars = fmincon(@(x) CostFcn(x, r0, v0, mu, Launcher, r_earth), ...
    optVars0,[],[],[],[],lw_bound,up_bound, ...
    @(x) ConstFcn(x, r0, v0, mu, Launcher, r_earth), options);

% Optimal trajectory
tb = optVars(1)*1e2;
pr0 = optVars(2:4);
pv0 = optVars(5:7);
init_state = [r0;v0;pr0;pv0];
options = odeset('RelTol',1e-8,'AbsTol',1e-8,'Events',@EndApo);

%% Burning
[t_burn, state_burn] = ode45(@(t,x) ExoatmosDyn(t,x,mu,Launcher.m0,...
    Launcher.mdot,Launcher.T),[0 tb], ...
    init_state,options); 
state_burn = state_burn';
t_burn = t_burn' + t_staging(end);

r_burn = zeros(length(t_burn),1);
for i = 1:length(t_burn)
   r_burn(i) = norm(state_burn(1:3,i));    
end
Mass.burn = Launcher.m0 - Launcher.mdot*(t_burn - t_staging(end));


% Balistic up to apoapsis
init_state = state_burn(:,end);
m = Launcher.m0 - Launcher.mdot*tb;


[t_ap, state_ap] = ode45(@(t,x) ExoatmosDyn(t,x,mu,m,...
    0,0),[tb 1e4], ...
    init_state,options);
state_ap = state_ap';
t_ap = t_ap' + t_staging(end);

Mass.ap = Mass.burn(end)*ones(1,length(t_ap));

r_ap = zeros(length(t_ap),1);
for i = 1:length(t_ap)
   r_ap(i) = norm(state_ap(1:3,i));    
end

% figure()
% plot(t_burn,r_burn,t_ap,r_ap);

% Balisitic flight
r_end_burn = state_burn(1:3,end);
v_end_burn = state_burn(4:6,end);

[a,e,~,~,~,~] = rv2coe(r_end_burn,v_end_burn,mu);

ap = a*(1+e);

E = norm(v_end_burn)^2/2 - mu/norm(r_end_burn);

v_ap = sqrt(2*(E + mu/ap));
v_end = sqrt(mu/ap);

dv = (v_end - v_ap)*1e3;
m = m*(1 - exp(-dv/Launcher.isp/g0));

% Orbit inyection
r_orbit = state_ap(1:3,end);
v_orbit = state_ap(4:6,end);

v_orbit = v_orbit/norm(v_orbit)*v_end;
orbit_period = sqrt(norm(r_orbit)^3/mu)*2*pi;

init_state = [r_orbit;v_orbit;ones(6,1)];

options = odeset('RelTol',1e-8,'AbsTol',1e-8);

[t_orbit, state_orbit] = ode45(@(t,x) ExoatmosDyn(t,x,mu,m,...
    0,0),[t_ap(end) t_ap(end)+orbit_period], ...
    init_state, options);
state_orbit = state_orbit';
t_orbit = t_orbit' ;%+ t_turn(end);

Mass.orbit = m*ones(1,length(t_orbit));
%% Post-processing
h_staging = zeros(length(t_staging),1);
v_in_staging = zeros(length(t_staging),1);
v_rel_staging = zeros(length(t_staging),1);
fl_ang_staging = zeros(length(t_staging),1);

for i = 1:length(t_staging)
    h_staging(i) = norm(state_staging(1:3,i))-R_earth;
    v_in_staging(i) = norm(state_staging(4:6,i));
    v_rel_staging(i) = norm(state_staging(4:6,i) - ...
        cross(omega,state_staging(1:3,i)));
    
    up = state_staging(1:3,i)/norm(state_staging(1:3,i));
    v_point = ( state_staging(4:6,i) - ...
        cross(omega,state_staging(1:3,i)) )/ v_rel_staging(i);
    fl_ang_staging(i) = 90 -  acosd(dot(up,v_point));
end

h_burn = zeros(length(t_burn),1);
v_in_burn = zeros(length(t_burn),1);
v_rel_burn = zeros(length(t_burn),1);
fl_ang_burn = zeros(length(t_burn),1);

for i = 1:length(t_burn)
    h_burn(i) = norm(state_burn(1:3,i));
    v_in_burn(i) = norm(state_burn(4:6,i));
    v_rel_burn(i) = norm(state_burn(4:6,i) - ...
        cross(omega,state_burn(1:3,i)));
    
    up = state_burn(1:3,i)/norm(state_burn(1:3,i));
    v_point = ( state_burn(4:6,i) - ...
        cross(omega,state_burn(1:3,i)) )/ v_rel_burn(i);
    fl_ang_burn(i) = 90 -  acosd(dot(up,v_point));
end

h_ap = zeros(length(t_ap),1);
v_in_ap = zeros(length(t_ap),1);
v_rel_ap = zeros(length(t_ap),1);
fl_ang_ap= zeros(length(t_ap),1);

for i = 1:length(t_ap)
    h_ap(i) = norm(state_ap(1:3,i));
    v_in_ap(i) = norm(state_ap(4:6,i));
    v_rel_ap(i) = norm(state_ap(4:6,i) - ...
        cross(omega,state_ap(1:3,i)));
    
    up = state_ap(1:3,i)/norm(state_ap(1:3,i));
    v_point = ( state_ap(4:6,i) -...
        cross(omega,state_ap(1:3,i)) )/ v_rel_ap(i);
    fl_ang_ap(i) = 90 - acosd(dot(up,v_point));
end

h_orbit = zeros(length(t_orbit),1);
v_in_orbit = zeros(length(t_orbit),1);
v_rel_orbit = zeros(length(t_orbit),1);
fl_ang_orbit = zeros(length(t_burn),1);

for i = 1:length(t_orbit)
    h_orbit(i) = norm(state_orbit(1:3,i));
    v_in_orbit(i) = norm(state_orbit(4:6,i));
    v_rel_orbit(i) = norm(state_orbit(4:6,i) - ...
        cross(omega,state_orbit(1:3,i)));
    
    up = state_orbit(1:3,i)/norm(state_orbit(1:3,i));
    v_point = ( state_orbit(4:6,i) - ...
        cross(omega,state_orbit(1:3,i)) )/ v_rel_orbit(i);
    fl_ang_orbit(i) =  90 - acosd(dot(up,v_point));
end



%% Optimization

% Cost function
function cost = CostFcn(optVars, r0, v0, mu, Launcher, r_earth)
% optVars = [tb;pr0;pv0] where
% tb: final burn time
% pr0: initial costate position
% pv0: initial costate velocity
tb = optVars(1);
pr0 = optVars(2:4);
pv0 = optVars(5:7);

%% Computation of the trajectory 
init_state = [r0;v0;pr0;pv0];
options = odeset('RelTol',1e-8,'AbsTol',1e-8,'Events',@EndAp700);
% Burning
[t_burn, state_burn] = ode45(@(t,x) ExoatmosDyn(t,x,mu,Launcher.m0,...
    Launcher.mdot,Launcher.T),[0 tb*1e2], ...
    init_state,options);
state_burn = state_burn';
t_burn = t_burn';

% Balistic flight, get orbital parameters
r_end_burn = state_burn(1:3,end);
v_end_burn = state_burn(4:6,end);

[a,e,~,~,~,~] = rv2coe(r_end_burn,v_end_burn,mu);

ap = a*(1+e);

E = norm(v_end_burn)^2/2 - mu/norm(r_end_burn);

v_ap = sqrt(2*(E + mu/ap));
v_end = sqrt(mu/ap);
m = Launcher.m0 - Launcher.mdot*tb*1e2;

if v_ap < v_end
    dv = (v_end - v_ap)*1e3;
    cost = Launcher.mdot*tb*1e2 + m*( 1 - exp(-dv/Launcher.isp/9.81));
else 
    cost = inf;
end

end

% Constraints function
function [c,ceq] = ConstFcn(optVars, r0, v0, mu, Launcher, r_earth)
% optVars = [tb;pr0;pv0] where
% tb: final burn time
% pr0: initial costate position
% pv0: initial costate velocity
tb = optVars(1);
pr0 = optVars(2:4);
pv0 = optVars(5:7);

c = [];

%% Computation of the trajectory 
init_state = [r0;v0;pr0;pv0];
options = odeset('RelTol',1e-8,'AbsTol',1e-8,'Events',@EndAp700);
% Burning
[t, state] = ode45(@(t,x) ExoatmosDyn(t,x,mu,Launcher.m0,...
    Launcher.mdot,Launcher.T),[0 tb*1e2], ...
    init_state,options); 

% Balistic flight, get orbital parameters
r_end_burn = state(end,1:3);
v_end_burn = state(end,4:6);

[a,e,Omega,i,omega,theta] = rv2coe(r_end_burn,v_end_burn,mu);

ceq = [a*(1+e) - 700 - r_earth;] ;
end

function [value, isterminal, dir] = EndAp700(t,state)
    isterminal = 1; % Finish integration after raising
    r = state(1:3);
    v = state(4:6);
    
    mu = 3.986004418e5;
    
    [a,e,~,~,~,~] = rv2coe(r,v,mu);
    
    ap = a*(1+e);
    
    value = ap - 700 - 6371; % Finish when the height reaches 100m
    dir = 1;
end

function [value, isterminal, dir] = EndApo(t,state)
    isterminal = 1; % Finish integration after raising
    dir = 0;
    r = state(1:3);
    v = state(4:6);
    
    value = dot(r,v);
end
%% Practice 2: Dynamics of the launcher
%
% Universidad Carlos III de Madrid
% Master in Space Engineering
% Launchers & Reentry
%
% Authors:  Angel del Pino Jiménez          (100439358) 
%           David Alejando Rubio Woodruff   (100363403)
%           Raúl Manuel Sánchez Moreno      (100363363)
%
% Date: October 2021
%

%% Parameters
% Earth
omega = [0; 0; 360/(23.9344*3600)*pi/180]; % Rotation vector of the Earth [rad/s]
mu = 3.986004418e14; % Gravitational parameter Earth [m3/s2]
g0 = 9.81; % Gravitational acc. at the surface [m/s2]
R_earth = 6371e3; % Earth's Radius [m]

% Data from the launcher
m_fairing = 50; % Fairing mass [kg]

Launcher.Mach = [0; 0.2; 0.5; 0.8; 1.2; 1.5; 1.75; 2; 2.25; 2.5; ...
    2.75; 3; 3.5; 4; 4.5; 5; 5.5; 6; 6.5];
Launcher.CD = [0.27; 0.27; 0.26; 0.25; 0.5; 0.46; 0.44; 0.41; 0.39; ...
    0.37; 0.35; 0.33; 0.3; 0.28; 0.26; 0.24; 0.23; 0.22; 0.21];
Launcher.cd = @(mach) spline(Launcher.Mach,Launcher.CD,mach);

Launcher.S_aero = 2; % Ref. Surface drag [m2]
Launcher.S_nozzle = 0.3; % Area nozzle [m2]
Launcher.exit_press = 40e3; % Nozzle eit pressure [Pa]

g_max = 7*g0;              % Maximum acceleration the launcher can withstand
Launcher.T = m_f(1)*g_max; % Launcher's thrust [N]
Launcher.init_mass = m_0(1) + m_fairing; % Initial mass of the launcher [kg]
Launcher.mass_flow = Launcher.T/isp(1)/g0; % Massflow launcher [kg/s]

%% Trajectory
% ECI reference frame is used!!

% Initial conditions 
Kuru.latitude = 5.159700; % Kuru's latitude [deg]
Kuru.longitude = -52.650299; % Kuru's longitude [deg]

[r0(1,1), r0(2,1), r0(3,1)] = ...
    sph2cart(deg2rad(Kuru.longitude),deg2rad(Kuru.latitude), 6371000);

r0_norm = r0/norm(r0);

v0 = cross(omega,r0); % Initial velocity
init_state = [r0; v0];

%% Dynamics 
% First stage (raising)
flag_flight = 1;

ToF = m_p(1)/Launcher.mass_flow; 

options = odeset('RelTol', 1e-8, 'AbsTol', 1e-12,'Events', @EndRaising200); 
[t_raising,state_raising] = ode45(@(t,state) EndoatmosDyn(t,state,Launcher,...
    omega, mu, R_earth, flag_flight), [0,ToF], init_state, options ) ;
t_raising = t_raising';
state_raising = state_raising';

% Kick-angle discontinuity
kick_angle = 0.1; % Kick angle [deg]

t_kick = t_raising(end);
r_kick = state_raising(1:3,end);
vKick_1 = state_raising(4:6,end);
vRel_1 =  vKick_1 - cross(omega,r_kick);

up = r_kick/norm(r_kick);
east = cross(omega/norm(omega),up);
east = east/norm(east);
north = cross(up,east);
north = north/norm(north);

vRel_2 = norm(vRel_1)*(cosd(kick_angle)*up + sind(kick_angle)*east);
vKick_2 = vRel_2 + cross(omega,state_raising(1:3,end));

% Second stage (gravity turn)
flag_flight = 2;
init_state = [r_kick ;vKick_2];

options = odeset('RelTol', 1e-8, 'AbsTol', 1e-12);
[t_turn,state_turn] = ode45(@(t,state) EndoatmosDyn(t, state, Launcher, ...
    omega, mu, R_earth,  flag_flight), [t_raising(end),ToF], ...
    init_state, options ) ;
t_turn = t_turn';
state_turn = state_turn';

% Post-processing
h_raising = zeros(length(t_raising),1);
v_in_raising = zeros(length(t_raising),1);
v_rel_raising = zeros(length(t_raising),1);
fl_ang_raising = zeros(length(t_raising),1);
dyn_press_raising = zeros(length(t_raising),1);

for i = 1:length(t_raising)
    h_raising(i) = norm(state_raising(1:3,i)) - R_earth;
    v_in_raising(i) = norm(state_raising(4:6,i));
    v_rel_raising(i) = norm(state_raising(4:6,i) - ...
        cross(omega,state_raising(1:3,i)));
    
    up = state_raising(1:3,i)/norm(state_raising(1:3,i));
    v_point = ( state_raising(4:6,i) - ...
        cross(omega,state_raising(1:3,i)) )/ v_rel_raising(i);
    fl_ang_raising(i) =  90 - acosd(dot(up,v_point));
    
    [dens,press,~,a] = expEarthAtm(h_raising(i),0);
    dyn_press_raising(i) = 0.5*dens*v_rel_raising(i)^2;
end

h_turn = zeros(length(t_turn),1);
v_in_turn = zeros(length(t_raising),1);
v_rel_turn = zeros(length(t_raising),1);
fl_ang_turn = zeros(length(t_raising),1);
dyn_press_turn = zeros(length(t_raising),1);

for i = 1:length(t_turn)
    h_turn(i) = norm(state_turn(1:3,i)) - R_earth;
    v_in_turn(i) = norm(state_turn(4:6,i));
    v_rel_turn(i) = norm(state_turn(4:6,i) - ...
        cross(omega,state_turn(1:3,i)));
    
    up = state_turn(1:3,i)/norm(state_turn(1:3,i));
%     v_point = state_turn(4:6,i)/v_in_turn(i);
    v_point = ( state_turn(4:6,i) - ...
        cross(omega,state_turn(1:3,i)) )/ v_rel_turn(i);
    fl_ang_turn(i) = 90 - acosd(dot(up,v_point));
    
    [dens,press,~,a] = expEarthAtm(h_turn(i),0);
    dyn_press_turn(i) = 0.5*dens*v_rel_turn(i)^2;
end

% Mass over stages
Mass.raising = m_0(1) - Launcher.mass_flow * t_raising;
Mass.turn = m_0(1) - Launcher.mass_flow * t_turn;

init_state = state_turn(:,end);
%% Plots 
% views
% clearvars -except m_0 m_p m_f isp init_state mu R_earth g0 omega g_max

%%  Events
function [value, isterminal, dir] = EndRaising200(t,state)
    isterminal = 1; % Finish integration after raising
    value = norm(state(1:3)) - 150 - 6371e3; % Finish when the height reaches 100m
    dir = 1;
end

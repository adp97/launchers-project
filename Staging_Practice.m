%% Practice 1: Launchers optimization assignment
%
% Universidad Carlos III de Madrid
% Master in Space Engineering
% Launchers & Reentry
%
% Authors:  Angel del Pino Jiménez          (100439358) 
%           David Alejando Rubio Woodruff   (100363403)
%           Raúl Manuel Sánchez Moreno      (100363363)
%
% Date: October 2021
%

%% Step 1: Solve the staging problem

g0 = 9.81/1000;     % Gravity acc. at Earth Surface [km/s2]
mu_earth = 3.986e5; % Gravitational parameter [km3/s2]
r_earth  = 6370;    % Radius of the Earth [km]  

mass_payload = 300; % Mass payload [kg]
height_orbit = 700; % Height of the final orbit [km]

v_orbit = sqrt(mu_earth/(r_earth + height_orbit));
dv_mission = v_orbit + 2;   % Dv for the mission: speed at the final orbit 
                            % plus 2 km/s

N = 2;             % Number of stages
eps = [0.1; 0.13]; % Structural coeffs
isp = [300; 320];  % Specific Impulses [s]

% Initial guess of the lagrange multiplier
lag0 = 1/(g0*mean(isp)*(exp(dv_mission/N/g0/mean(isp))*mean(eps)-1));

% Finding the lagrange multiplier
f = @(lag) g0 * sum( isp.*log((1 + lag*g0*isp)./(lag*g0*isp.*eps)) ) - ...
    dv_mission;
lag = fzero(f,lag0);

% Get mass distribution
mass_ratio_fun = @(isp,eps,lag) (1 + lag*g0*isp)/(lag*g0*isp*eps);
dvi_fun = @(isp,eps,Lmd) g0*isp*log(Lmd + eps*(1-Lmd));
payload_ratio_fun = @(eps,mass_ratio) (1-mass_ratio*eps)/(1-eps)/mass_ratio;
prop_ratio_fun = @(eps,payload_ratio) (1-eps)*(1-payload_ratio);

mass_ratio = zeros(length(eps),1);
prop_ratio = zeros(length(eps),1);
payload_ratio = zeros(length(eps),1);
dvi = zeros(length(eps),1);

for i = 1:length(eps)
   mass_ratio(i) = mass_ratio_fun(isp(i),eps(i),lag);
   dvi(i) = dvi_fun(isp(i),eps(i),mass_ratio(i));
   payload_ratio(i) = payload_ratio_fun(eps(i),mass_ratio(i));
   prop_ratio(i) = prop_ratio_fun(eps(i),payload_ratio(i));
end

% Check maximum

for i = 1:length(eps)
    snd_der = - (1+lag*g0*isp(i))/mass_ratio(i)^2 ...
        + (eps(i)/(1-eps(i)*mass_ratio(i)))^2;
    if snd_der <= 0
        fprintf('Warning, this is not a maximum!\n');
        break
    end
end

% Print results

% m01 m02 mf1 mf2 mp1 mp2 

% stage 2
m_0(2) = mass_payload/payload_ratio(2);     % initial mass  (kg)
m_p(2) = m_0(2)*prop_ratio(2);              % propellant mass  (kg)
m_f(2) = m_0(2) - m_p(2);                   % final mass  (kg)

% stage 1
m_0(1) = m_0(2)/payload_ratio(1);
m_p(1) = m_0(1)*prop_ratio(1);
m_f(1) = m_0(1) - m_p(1);

disp('STAGING PRACTICE RESULTS:')
fprintf('Payload mass: %g kg \n',mass_payload)
fprintf('\nStage 1:')
fprintf('\n Initial mass: %g kg \n Propellant mass: %g kg\n Dry mass: %g kg\n',...
    m_0(1),m_p(1),m_f(1))
fprintf(' Struct mass: %g kg\n',eps(1)/(1-eps(1))*m_p(1));

fprintf('\n\nStage 2:')
fprintf('\n Initial mass: %g kg \n Propellant mass: %g kg\n Dry mass: %g kg\n',...
    m_0(2),m_p(2),m_f(2))
fprintf(' Struct mass: %g kg\n',eps(2)/(1-eps(2))*m_p(2));
fprintf('\n\n')

% Keep just required variables in workspace
% clearvars -except m_0 m_p m_f isp